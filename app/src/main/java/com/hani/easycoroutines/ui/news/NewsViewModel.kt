package com.hani.easycoroutines.ui.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hani.easycoroutines.data.models.NewsResponse
import com.hani.easycoroutines.data.models.Output
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class NewsViewModel(private val newsRepo: NewsRepo) : ViewModel() {

    //create a new Job
    private val parentJob = Job()

    //create a coroutine context with the job and the dispatcher
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.IO

    private val scope = CoroutineScope(coroutineContext)


    fun getLatestNews(): MutableLiveData<Output<NewsResponse>> {

        val newsLiveData = MutableLiveData<Output<NewsResponse>>()

        ///launch the coroutine scope
        scope.launch {
            //get latest news from news repo
            val latestNewsOutput = newsRepo.getLatestNews()
            //post the value inside live data
            newsLiveData.postValue(latestNewsOutput)
        }

        return newsLiveData
    }


    override fun onCleared() {
        super.onCleared()

        coroutineContext.cancel()
    }
}