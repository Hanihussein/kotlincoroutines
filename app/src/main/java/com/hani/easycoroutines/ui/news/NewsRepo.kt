package com.hani.easycoroutines.ui.news

import com.hani.easycoroutines.api.NewsServices
import com.hani.easycoroutines.data.base.BaseRepository
import com.hani.easycoroutines.data.models.NewsResponse
import com.hani.easycoroutines.data.models.Output

class NewsRepo(apiInterface: NewsServices) : BaseRepository(apiInterface) {


    suspend fun getLatestNews(): Output<NewsResponse>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.fetchLatestNewsAsync("Nigeria", "publishedAt").await() },
            error = "Error fetching news"
            //convert to mutable list
        )
    }
}