package com.hani.easycoroutines.ui.news

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.hani.easycoroutines.R
import com.hani.easycoroutines.data.models.NewsResponse
import com.hani.easycoroutines.data.models.Output
import org.koin.android.viewmodel.ext.android.viewModel

class NewsActivity : AppCompatActivity() {


    private val newsViewModel: NewsViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getLatestNews()
    }


    private fun getLatestNews() {

        newsViewModel.getLatestNews().observe(this, object : Observer<Output<NewsResponse>> {

            override fun onChanged(t: Output<NewsResponse>?) {

                when (t) {

                    is Output.Success -> {

                        Log.i("Response", t.output.toString())
                    }

                    is Output.Error -> {

                        Log.i("Response", t.exception.toString())

                    }
                }
            }
        })
    }
}
