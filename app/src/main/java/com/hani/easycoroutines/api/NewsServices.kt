package com.hani.easycoroutines.api

import com.hani.easycoroutines.data.models.NewsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsServices {

    companion object {
        const val ENDPOINT = "https://newsapi.org/"
        const val API_KEY = "f1e5ca69296b4e70a3fb7fc722a63615"

    }


    @GET("v2/everything/")
    fun fetchLatestNewsAsync(@Query("q") query: String,
                             @Query("sortBy") sortBy : String) : Deferred<Response<NewsResponse>>


}