package com.hani.easycoroutines.di

import com.hani.easycoroutines.api.NewsServices
import com.hani.easycoroutines.ui.news.NewsRepo
import com.hani.easycoroutines.ui.news.NewsViewModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


private val retrofit: Retrofit = createNetworkClient()


fun createNetworkClient(): Retrofit {

    return Retrofit.Builder().client(apiClient)
        .baseUrl(NewsServices.ENDPOINT)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

private val interceptor
    get() = Interceptor.invoke {

        val url =
            it.request().url.newBuilder().addQueryParameter("apiKey", NewsServices.API_KEY).build()
        val request = it.request()
            .newBuilder()
            .url(url)
            .build()
        it.proceed(request)

    }

private val apiClient
    get() = OkHttpClient().newBuilder().addInterceptor(interceptor).build()


private val newsAPI: NewsServices = retrofit.create(NewsServices::class.java)


val repoModule = module {
    single { NewsRepo(get()) }
}


val viewModelModule = module {
    viewModel { NewsViewModel(get()) }
}


val networkModule = module {
    single { newsAPI }
}


val databaseModule = module {

}

val sharedPrefModule = module {

}