package com.hani.easycoroutines.data.base

import com.hani.easycoroutines.api.NewsServices
import com.hani.easycoroutines.data.models.Output
import retrofit2.Response
import java.io.IOException

open class BaseRepository(protected val apiInterface: NewsServices) {


    suspend fun <T : Any> safeApiCall(call : suspend()-> Response<T>, error : String) :  Output<T>?{
        val result = newsApiOutput(call, error)
//        when(result){
//            is Output.Success ->
//                output = result.output
//            is Output.Error -> Log.e("Error", "The $error and the ${result.exception}")
//        }
        return result

    }


    private suspend fun <T : Any> newsApiOutput(
        call: suspend () -> Response<T>,
        error: String
    ): Output<T> {

        val response = call.invoke()

        return if (response.isSuccessful)
            Output.Success(response.body()!!)
        else
            Output.Error(IOException("OOps .. Something went wrong due to  $error"))


    }
}