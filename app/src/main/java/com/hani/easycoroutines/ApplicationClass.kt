package com.hani.easycoroutines

import android.app.Application
import com.hani.easycoroutines.di.networkModule
import com.hani.easycoroutines.di.repoModule
import com.hani.easycoroutines.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

open class ApplicationClass : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {

            androidContext(this@ApplicationClass)

            modules(listOf(viewModelModule, repoModule, networkModule))

        }
    }

}




